<?php
/**
 * 登录、注册
 */
if (!session_id()) session_start();
class Api_Login extends PhalApi_Api { 
	public function getRules() {
        return array(
			'userLogin' => array(
                'country_code' => array('name' => 'country_code', 'type' => 'int', 'default'=>'86','require' => true,  'desc' => '国家代号'),
                'user_login' => array('name' => 'user_login', 'type' => 'string', 'require' => true,  'min' => '6',  'max'=>'30', 'desc' => '账号'),
				'user_pass' => array('name' => 'user_pass', 'type' => 'string','require' => true,  'min' => '1',  'max'=>'30', 'desc' => '密码'),
				
            ),
			'userReg' => array(
                'country_code' => array('name' => 'country_code', 'type' => 'int','default'=>'86', 'require' => true,  'desc' => '国家代号'),
                'user_login' => array('name' => 'user_login', 'type' => 'string','require' => true,  'min' => '6',  'max'=>'30', 'desc' => '账号'),
				'user_pass' => array('name' => 'user_pass', 'type' => 'string','require' => true,  'min' => '1',  'max'=>'30', 'desc' => '密码'),
				'user_pass2' => array('name' => 'user_pass2', 'type' => 'string',  'require' => true,  'min' => '1',  'max'=>'30', 'desc' => '确认密码'),
                'code' => array('name' => 'code', 'type' => 'string', 'min' => 1, 'require' => true,   'desc' => '验证码'),
                'source' => array('name' => 'source', 'type' => 'string',  'default'=>'pc', 'desc' => '来源设备'),
                'source_type' => array('name' => 'source_type', 'type' => 'int',  'default'=>'0', 'desc' => '0：直播demo；1：小程序'),
            ),
			'userFindPass' => array(
                'country_code' => array('name' => 'country_code', 'type' => 'int','default'=>'86', 'require' => true,  'desc' => '国家代号'),
                'user_login' => array('name' => 'user_login', 'type' => 'string', 'require' => true,  'min' => '6',  'max'=>'30', 'desc' => '账号'),
				'user_pass' => array('name' => 'user_pass', 'type' => 'string', 'require' => true,  'min' => '1',  'max'=>'30', 'desc' => '密码'),
				'user_pass2' => array('name' => 'user_pass2', 'type' => 'string', 'require' => true,  'min' => '1',  'max'=>'30', 'desc' => '确认密码'),
                'code' => array('name' => 'code', 'type' => 'string', 'min' => 1, 'require' => true,   'desc' => '验证码'),
                'source_type' => array('name' => 'source_type', 'type' => 'int',  'default'=>'0', 'desc' => '0：直播demo；1：小程序'),
            ),	
			
			'getCode' => array(
                'country_code' => array('name' => 'country_code', 'type' => 'int','default'=>'86', 'require' => true,  'desc' => '国家代号'),
				'mobile' => array('name' => 'mobile', 'type' => 'string', 'min' => 1, 'require' => true,  'desc' => '手机号'),
			),
			
			'getForgetCode' => array(
                'country_code' => array('name' => 'country_code', 'type' => 'int','default'=>'86', 'require' => true,  'desc' => '国家代号'),
				'mobile' => array('name' => 'mobile', 'type' => 'string', 'min' => 1, 'require' => true,  'desc' => '手机号'),
			),


            'logout' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户Token'),
			),

        );
	}
	
    /**
     * 会员登陆 需要密码
     * @desc 用于用户登陆信息
     * @return int code 操作码，0表示成功
     * @return array info 用户信息
     * @return string info[0].id 用户ID
     * @return string info[0].user_nicename 昵称
     * @return string info[0].avatar 头像
     * @return string info[0].avatar_thumb 头像缩略图
     * @return string info[0].sex 性别
     * @return string info[0].signature 签名
     * @return string info[0].coin 用户余额
     * @return string info[0].login_type 注册类型
     * @return string info[0].level 等级
     * @return string info[0].province 省份
     * @return string info[0].city 城市
     * @return string info[0].birthday 生日
     * @return string info[0].token 用户Token
     * @return string msg 提示信息
     */
    public function userLogin() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

		$country_code=checkNull($this->country_code);
        $user_login=checkNull($this->user_login);
		$user_pass=checkNull($this->user_pass);

        $domain = new Domain_Login();
        $info = $domain->userLogin($country_code,$user_login,$user_pass);

		if($info==1001){
			$rs['code'] = 1001;
            $rs['msg'] = '账号或密码错误';
            return $rs;	
		}else if($info==1002){
			$rs['code'] = 1002;
			//禁用信息
			$baninfo=$domain->getUserban($user_login);
            $rs['info'][0] =$baninfo;
            return $rs;	
		}else if($info==1003){
			$rs['code'] = 1003;
            $rs['msg'] = '该账号已被禁用';
            return $rs;	
		}else if($info==1004){
            $rs['code'] = 1004;
            $rs['msg'] = '该账号已注销';
            return $rs; 
        }else if($info==1005){
            $rs['code'] = 1005;
            $rs['msg'] = '请先下麦再登录';
            return $rs; 
        }


	
        $rs['info'][0] = $info;
        
        
				
		
        return $rs;
    }		
   /**
     * 会员注册
     * @desc 用于用户注册信息
     * @return int code 操作码，0表示成功
     * @return array info 用户信息
     * @return string info[0].id 用户ID
     * @return string info[0].user_nicename 昵称
     * @return string info[0].avatar 头像
     * @return string info[0].avatar_thumb 头像缩略图
     * @return string info[0].sex 性别
     * @return string info[0].signature 签名
     * @return string info[0].coin 用户余额
     * @return string info[0].login_type 注册类型
     * @return string info[0].level 等级
     * @return string info[0].province 省份
     * @return string info[0].city 城市
     * @return string info[0].birthday 生日
     * @return string info[0].token 用户Token
     * @return string msg 提示信息
     */
    public function userReg() {

        $rs = array('code' => 0, 'msg' => '注册成功', 'info' => array());
	   
		$country_code=checkNull($this->country_code);
        $user_login=checkNull($this->user_login);
		$user_pass=checkNull($this->user_pass);
		$user_pass2=checkNull($this->user_pass2);
		$source=checkNull($this->source);
		$code=checkNull($this->code);
        $source_type=checkNull($this->source_type);

        if($source_type!='1'){

            if(!$_SESSION['reg_mobile'] || !$_SESSION['reg_mobile_code']){
                $rs['code'] = 1001;
                $rs['msg'] = '请先获取验证码';
                return $rs;     
            }

            if($country_code!=$_SESSION['country_code']){
                $rs['code'] = 1001;
                $rs['msg'] = '国家不一致';
                return $rs;                 
            }

        
            if($user_login!=$_SESSION['reg_mobile']){
                $rs['code'] = 1001;
                $rs['msg'] = '手机号码不一致';
                return $rs;                 
            }

            if($code!=$_SESSION['reg_mobile_code']){
                $rs['code'] = 1002;
                $rs['msg'] = '验证码错误';
                return $rs;                 
            }   

        }
        
        

		if($user_pass!=$user_pass2){
            $rs['code'] = 1003;
            $rs['msg'] = '两次输入的密码不一致';
            return $rs;					
		}	
        
		$check = passcheck($user_pass);

		if(!$check){
            $rs['code'] = 1004;
            $rs['msg'] = '密码为6-20位字母数字组合';
            return $rs;										
        }
        
		$domain = new Domain_Login();
		$info = $domain->userReg($country_code,$user_login,$user_pass,$source);

		if($info==1006){
			$rs['code'] = 1006;
            $rs['msg'] = '该手机号已被注册！';
            return $rs;	
		}else if($info==1007){
			$rs['code'] = 1007;
            $rs['msg'] = '注册失败，请重试';
            return $rs;	
		}

        $rs['info'][0] = $info;
		
		$_SESSION['reg_mobile'] = '';
		$_SESSION['reg_mobile_code'] = '';
		$_SESSION['reg_mobile_expiretime'] = '';
			
        return $rs;
    }		
	/**
     * 会员找回密码
     * @desc 用于会员找回密码
     * @return int code 操作码，0表示成功，1表示验证码错误，2表示用户密码不一致,3短信手机和登录手机不一致 4、用户不存在 801 密码6-12位数字与字母
     * @return array info 
     * @return string msg 提示信息
     */
    public function userFindPass() {
		
        $rs = array('code' => 0, 'msg' => '密码找回成功', 'info' => array());
		
		$country_code=checkNull($this->country_code);
        $user_login=checkNull($this->user_login);
		$user_pass=checkNull($this->user_pass);
		$user_pass2=checkNull($this->user_pass2);
		$code=checkNull($this->code);
        $source_type=checkNull($this->source_type);//0:直播demo；1：小程序

        if($source_type!='1'){

            if(!$_SESSION['forget_country_code'] || !$_SESSION['forget_mobile'] || !$_SESSION['forget_mobile_code']){
                $rs['code'] = 1001;
                $rs['msg'] = '请先获取验证码';
                return $rs;     
            }

            if($country_code!=$_SESSION['forget_country_code']){
                $rs['code'] = 1001;
                $rs['msg'] = '国家不一致';
                return $rs;                 
            }
            
            if($user_login!=$_SESSION['forget_mobile']){
                $rs['code'] = 1001;
                $rs['msg'] = '手机号码不一致';
                return $rs;                 
            }

            if($code!=$_SESSION['forget_mobile_code']){
                $rs['code'] = 1002;
                $rs['msg'] = '验证码错误';
                return $rs;                 
            }

        }
		
			
		

		if($user_pass!=$user_pass2){
            $rs['code'] = 1003;
            $rs['msg'] = '两次输入的密码不一致';
            return $rs;					
		}	

		$check = passcheck($user_pass);
		if(!$check){
            $rs['code'] = 1004;
            $rs['msg'] = '密码为6-20位字母数字组合';
            return $rs;										
        }	

		$domain = new Domain_Login();
        $info = $domain->userFindPass($country_code,$user_login,$user_pass);	
		
		if($info==1006){
			$rs['code'] = 1006;
            $rs['msg'] = '该帐号不存在';
            return $rs;	
		}else if($info===false){
			$rs['code'] = 1007;
            $rs['msg'] = '重置失败，请重试';
            return $rs;	
		}
		
        $_SESSION['forget_mobile_code'] = '';
		$_SESSION['forget_mobile'] = '';
		$_SESSION['forget_mobile_code'] = '';
		$_SESSION['forget_mobile_expiretime'] = '';

        return $rs;
    }
	
	
	/**
	 * 获取注册短信验证码
	 * @desc 用于注册获取短信验证码
	 * @return int code 操作码，0表示成功,2发送失败
	 * @return array info 
	 * @return string msg 提示信息
	 */
	 
	public function getCode() {
		$rs = array('code' => 0, 'msg' => '发送成功', 'info' => array(),"verificationcode"=>0);
		
		$country_code = checkNull($this->country_code);
        $mobile = checkNull($this->mobile);

        $configpri=getConfigPri();
     

		

        $ismobile=checkMobile($mobile);
		if(!$ismobile){
			$rs['code']=1001;
			$rs['msg']='请输入正确的手机号';
			return $rs; 
		}
        $where="country_code='{$country_code}' and user_login='{$mobile}'";
        
		$checkuser = checkUser($where);	
        
        if($checkuser){
            $rs['code']=1004;
			$rs['msg']='该手机号已注册';
			return $rs;
        }
		
	
		if($_SESSION['country_code']==$country_code && $_SESSION['reg_mobile']==$mobile && $_SESSION['reg_mobile_expiretime']> time() ){
			$rs['code']=1002;
			$rs['msg']='验证码5分钟有效，请勿多次发送';
			return $rs;
		}
		
        $limit = ip_limit();	
		if( $limit == 1){
			$rs['code']=1003;
			$rs['msg']='您当日已发送次数过多';
			return $rs;
		}		
		$mobile_code = random(6,1);
		
		/* 发送验证码 */
 		$result=sendCode($country_code,$mobile,$mobile_code);
		if($result['code']==0){
            $rs['verificationcode']=$mobile_code;
            $_SESSION['country_code'] = $country_code;
			$_SESSION['reg_mobile'] = $mobile;
			$_SESSION['reg_mobile_code'] = $mobile_code;
			$_SESSION['reg_mobile_expiretime'] = time() +60*5;	
		}else if($result['code']==667){
			$_SESSION['country_code'] = $country_code;
            $_SESSION['reg_mobile'] = $mobile;
            $_SESSION['reg_mobile_code'] = '123456';
            $_SESSION['reg_mobile_expiretime'] = time() +60*5;
			
            
            $rs['verificationcode']='123456';
            $rs['code']=0;
			$rs['msg']='验证码为：'.$result['msg'];
		}else{
			$rs['code']=1002;
			$rs['msg']=$result['msg'];
		} 
		
		
		return $rs;
	}		

	/**
	 * 获取找回密码短信验证码
	 * @desc 用于找回密码获取短信验证码
	 * @return int code 操作码，0表示成功,2发送失败
	 * @return array info 
	 * @return string msg 提示信息
	 */
	 
	public function getForgetCode() {
		$rs = array('code' => 0, 'msg' => '发送成功', 'info' => array(),"verificationcode"=>0);
		
		$country_code = checkNull($this->country_code);
        $mobile = checkNull($this->mobile);
		
        $configpri=getConfigPri();
        $typecode_switch=$configpri['typecode_switch'];

        if($typecode_switch==1){ //阿里云验证码

            $aly_sendcode_type=$configpri['aly_sendcode_type'];

            if($aly_sendcode_type==1){ //国内验证码
                if($country_code!=86){
                    $rs['code']=1001;
                    $rs['msg']='平台只允许选择中国大陆';
                    return $rs;
                }

                $ismobile=checkMobile($mobile);
                if(!$ismobile){
                    $rs['code']=1001;
                    $rs['msg']='请输入正确的手机号';
                    return $rs; 
                }

            }else if($aly_sendcode_type==2){ //海外/港澳台 验证码
                if($country_code==86){
                    $rs['code']=1001;
                    $rs['msg']='平台只允许选择除中国大陆外的国家/地区';
                    return $rs;
                }
            }
        }else if($typecode_switch==2){ //容联云

            $ismobile=checkMobile($mobile);
            if(!$ismobile){
                $rs['code']=1001;
                $rs['msg']='请输入正确的手机号';
                return $rs; 
            }
        }else if($typecode_switch==3){ //腾讯云
            
            $tencent_sendcode_type=$configpri['tencent_sendcode_type'];
            if($tencent_sendcode_type==1){ //中国大陆
                if($country_code!=86){
                    $rs['code']=1001;
                    $rs['msg']='平台只允许选择中国大陆';
                    return $rs;
                }

                $ismobile=checkMobile($mobile);
                if(!$ismobile){
                    $rs['code']=1001;
                    $rs['msg']='请输入正确的手机号';
                    return $rs; 
                }
            }else if($tencent_sendcode_type==2){ //海外/港澳台 验证码
                if($country_code==86){
                    $rs['code']=1001;
                    $rs['msg']='平台只允许选择除中国大陆外的国家/地区';
                    return $rs;
                }
            }
        }
		
    
        
        $where="country_code='{$country_code}' and user_login='{$mobile}'";
        $checkuser = checkUser($where);	
        
        if(!$checkuser){
            $rs['code']=1004;
			$rs['msg']='该手机号未注册';
			return $rs;
        }

        //判断手机号是否注销
        $is_destroy=checkIsDestroyByLogin($country_code,$mobile);
        if($is_destroy){
            $rs['code']=1005;
            $rs['msg']='该手机号已注销';
            return $rs;
        }

		if($_SESSION['forget_country_code']==$country_code && $_SESSION['forget_mobile']==$mobile && $_SESSION['forget_mobile_expiretime']> time() ){
			$rs['code']=1002;
			$rs['msg']='验证码5分钟有效，请勿多次发送';
			return $rs;
		}

        $limit = ip_limit();	
		if( $limit == 1){
			$rs['code']=1003;
			$rs['msg']='您已当日发送次数过多';
			return $rs;
		}	
		$mobile_code = random(6,1);
		
		/* 发送验证码 */
 		$result=sendCode($country_code,$mobile,$mobile_code);
		if($result['code']==0){
            $rs['verificationcode']=$mobile_code;
			$_SESSION['forget_country_code'] = $country_code;
            $_SESSION['forget_mobile'] = $mobile;
			$_SESSION['forget_mobile_code'] = $mobile_code;
			$_SESSION['forget_mobile_expiretime'] = time() +60*5;	
		}else if($result['code']==667){
            $_SESSION['forget_country_code'] = $country_code;
			$_SESSION['forget_mobile'] = $mobile;
            $_SESSION['forget_mobile_code'] = $result['msg'];
            $_SESSION['forget_mobile_expiretime'] = time() +60*5;
            
            $rs['verificationcode']='123456';
            $rs['code']=1002;
			$rs['msg']='验证码为：'.$result['msg'];
		}else{
			$rs['code']=1002;
			$rs['msg']=$result['msg'];
		} 
		
		return $rs;
	}	
    
    
	/**
	 * 退出
	 * @desc 用于用户退出 注销极光
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string msg 提示信息
	 */
	public function logout() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid = checkNull($this->uid);
		$token=checkNull($this->token);
        
		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}

        

		$info = userLogout($uid);


		return $rs;			
	}


}
